<?php
// Routes

$app->get('/', function ($request, $response, $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    $feed = new \Froggler\Feed();
    $args['name'] = $feed->hello();

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});

// Routes

$app->get('/crawl', function ($request, $response, $args) {
    // Sample log message
    $this->logger->info("Froggler started crawling");

    $feed = new \Froggler\Feed();

    // Render index view
    return $this->renderer->render($response, 'crawl.phtml', $args);
});
